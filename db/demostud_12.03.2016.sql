-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.25 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table octobercms.backend_access_log
DROP TABLE IF EXISTS `backend_access_log`;
CREATE TABLE IF NOT EXISTS `backend_access_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.backend_access_log: ~6 rows (approximately)
/*!40000 ALTER TABLE `backend_access_log` DISABLE KEYS */;
INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
	(1, 1, '::1', '2016-02-18 09:25:27', '2016-02-18 09:25:27'),
	(2, 1, '::1', '2016-03-04 06:45:00', '2016-03-04 06:45:00'),
	(3, 1, '::1', '2016-03-12 06:12:57', '2016-03-12 06:12:57'),
	(4, 2, '::1', '2016-03-12 06:33:32', '2016-03-12 06:33:32'),
	(5, 1, '::1', '2016-03-12 13:29:41', '2016-03-12 13:29:41'),
	(6, 2, '::1', '2016-03-12 13:31:30', '2016-03-12 13:31:30'),
	(7, 1, '::1', '2016-03-12 14:00:00', '2016-03-12 14:00:00');
/*!40000 ALTER TABLE `backend_access_log` ENABLE KEYS */;


-- Dumping structure for table octobercms.backend_users
DROP TABLE IF EXISTS `backend_users`;
CREATE TABLE IF NOT EXISTS `backend_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_unique` (`login`),
  UNIQUE KEY `email_unique` (`email`),
  KEY `act_code_index` (`activation_code`),
  KEY `reset_code_index` (`reset_password_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.backend_users: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_users` DISABLE KEYS */;
INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `activated_at`, `last_login`, `created_at`, `updated_at`, `is_superuser`) VALUES
	(1, 'Admin', 'Person', 'admin', 'bhaumik25@gmail.com', '$2y$10$1Semh9H6vbXjGmOy8n1D3uyrxPp8QeL2plLM.pefZBN8E4rfmmzOi', NULL, '$2y$10$m8HkGrnby33uGLZFjarfeOSH/2YKamlnqq39RKRPC8xklo5TgDbSW', NULL, '', 1, NULL, '2016-03-12 13:59:59', '2016-02-18 09:23:43', '2016-03-12 13:59:59', 1),
	(2, 'Master', 'Admin', 'company', 'bhaumik25php@gmail.com', '$2y$10$Cd4iVZRYLBZOipTPN.rRVuzUfd39.Gd4lEWAVZGrEsuoScqWQ4Gay', NULL, '$2y$10$cLeYWeuIXMkdh2n7HA5x1.3iIbcv3bcpcIiVF70WarGD4pO4Yaa.m', NULL, '{"tb.events.access_batches":1,"rainlab.users.access_users":-1,"rainlab.users.access_groups":-1,"rainlab.users.access_settings":-1}', 0, NULL, '2016-03-12 13:31:29', '2016-03-12 06:33:06', '2016-03-12 13:31:29', 0);
/*!40000 ALTER TABLE `backend_users` ENABLE KEYS */;


-- Dumping structure for table octobercms.backend_users_groups
DROP TABLE IF EXISTS `backend_users_groups`;
CREATE TABLE IF NOT EXISTS `backend_users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.backend_users_groups: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_users_groups` DISABLE KEYS */;
INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
	(1, 1),
	(1, 2),
	(2, 3);
/*!40000 ALTER TABLE `backend_users_groups` ENABLE KEYS */;


-- Dumping structure for table octobercms.backend_user_groups
DROP TABLE IF EXISTS `backend_user_groups`;
CREATE TABLE IF NOT EXISTS `backend_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_unique` (`name`),
  KEY `code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.backend_user_groups: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_user_groups` DISABLE KEYS */;
INSERT INTO `backend_user_groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
	(1, 'Owners', NULL, '2016-02-18 09:23:42', '2016-02-18 09:23:42', 'owners', 'Default group for website owners.', 0),
	(2, 'Administrator', '{"cms.manage_pages":"1","media.manage_media":"1","cms.manage_content":"1","system.manage_mail_templates":"1","system.manage_mail_settings":"1","backend.access_dashboard":"1","system.access_logs":"1"}', '2016-02-25 06:38:07', '2016-02-25 06:38:07', 'administrator', 'Admministrator of system', 1),
	(3, 'company', '{"tb.events.access_batches":"1","backend.access_dashboard":"1"}', '2016-03-12 06:31:20', '2016-03-12 13:33:31', 'company', 'Comany Admin', 0);
/*!40000 ALTER TABLE `backend_user_groups` ENABLE KEYS */;


-- Dumping structure for table octobercms.backend_user_preferences
DROP TABLE IF EXISTS `backend_user_preferences`;
CREATE TABLE IF NOT EXISTS `backend_user_preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.backend_user_preferences: ~0 rows (approximately)
/*!40000 ALTER TABLE `backend_user_preferences` DISABLE KEYS */;
INSERT INTO `backend_user_preferences` (`id`, `user_id`, `namespace`, `group`, `item`, `value`) VALUES
	(1, 1, 'backend', 'reportwidgets', 'dashboard', '{"report_container_dashboard_1":{"class":"System\\\\ReportWidgets\\\\Status","configuration":{"title":"backend::lang.dashboard.status.widget_title_default","ocWidgetWidth":"4"},"sortOrder":1}}'),
	(2, 2, 'backend', 'reportwidgets', 'dashboard', '{"report_container_dashboard_1":{"class":"System\\\\ReportWidgets\\\\Status","configuration":{"title":"backend::lang.dashboard.status.widget_title_default","ocWidgetWidth":"10"},"sortOrder":1}}');
/*!40000 ALTER TABLE `backend_user_preferences` ENABLE KEYS */;


-- Dumping structure for table octobercms.backend_user_throttle
DROP TABLE IF EXISTS `backend_user_throttle`;
CREATE TABLE IF NOT EXISTS `backend_user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `backend_user_throttle_user_id_index` (`user_id`),
  KEY `backend_user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.backend_user_throttle: ~1 rows (approximately)
/*!40000 ALTER TABLE `backend_user_throttle` DISABLE KEYS */;
INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
	(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL),
	(2, 2, '::1', 0, NULL, 0, NULL, 0, NULL);
/*!40000 ALTER TABLE `backend_user_throttle` ENABLE KEYS */;


-- Dumping structure for table octobercms.cache
DROP TABLE IF EXISTS `cache`;
CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.cache: ~0 rows (approximately)
/*!40000 ALTER TABLE `cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cache` ENABLE KEYS */;


-- Dumping structure for table octobercms.cms_theme_data
DROP TABLE IF EXISTS `cms_theme_data`;
CREATE TABLE IF NOT EXISTS `cms_theme_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `cms_theme_data_theme_index` (`theme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.cms_theme_data: ~0 rows (approximately)
/*!40000 ALTER TABLE `cms_theme_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_theme_data` ENABLE KEYS */;


-- Dumping structure for table octobercms.deferred_bindings
DROP TABLE IF EXISTS `deferred_bindings`;
CREATE TABLE IF NOT EXISTS `deferred_bindings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `master_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `deferred_bindings_master_type_index` (`master_type`),
  KEY `deferred_bindings_master_field_index` (`master_field`),
  KEY `deferred_bindings_slave_type_index` (`slave_type`),
  KEY `deferred_bindings_slave_id_index` (`slave_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.deferred_bindings: ~0 rows (approximately)
/*!40000 ALTER TABLE `deferred_bindings` DISABLE KEYS */;
/*!40000 ALTER TABLE `deferred_bindings` ENABLE KEYS */;


-- Dumping structure for table octobercms.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;


-- Dumping structure for table octobercms.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;


-- Dumping structure for table octobercms.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.migrations: ~27 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2013_10_01_000001_Db_Deferred_Bindings', 1),
	('2013_10_01_000002_Db_System_Files', 1),
	('2013_10_01_000003_Db_System_Plugin_Versions', 1),
	('2013_10_01_000004_Db_System_Plugin_History', 1),
	('2013_10_01_000005_Db_System_Settings', 1),
	('2013_10_01_000006_Db_System_Parameters', 1),
	('2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
	('2013_10_01_000008_Db_System_Mail_Templates', 1),
	('2013_10_01_000009_Db_System_Mail_Layouts', 1),
	('2014_10_01_000010_Db_Jobs', 1),
	('2014_10_01_000011_Db_System_Event_Logs', 1),
	('2014_10_01_000012_Db_System_Request_Logs', 1),
	('2014_10_01_000013_Db_System_Sessions', 1),
	('2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
	('2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
	('2015_10_01_000016_Db_Cache', 1),
	('2015_10_01_000017_Db_System_Revisions', 1),
	('2015_10_01_000018_Db_FailedJobs', 1),
	('2013_10_01_000001_Db_Backend_Users', 2),
	('2013_10_01_000002_Db_Backend_User_Groups', 2),
	('2013_10_01_000003_Db_Backend_Users_Groups', 2),
	('2013_10_01_000004_Db_Backend_User_Throttle', 2),
	('2014_01_04_000005_Db_Backend_User_Preferences', 2),
	('2014_10_01_000006_Db_Backend_Access_Log', 2),
	('2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
	('2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
	('2014_10_01_000001_Db_Cms_Theme_Data', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_blog_categories
DROP TABLE IF EXISTS `rainlab_blog_categories`;
CREATE TABLE IF NOT EXISTS `rainlab_blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `rainlab_blog_categories_slug_index` (`slug`),
  KEY `rainlab_blog_categories_parent_id_index` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_blog_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_blog_categories` DISABLE KEYS */;
INSERT INTO `rainlab_blog_categories` (`id`, `name`, `slug`, `code`, `description`, `parent_id`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`) VALUES
	(1, 'Uncategorized', 'uncategorized', NULL, NULL, NULL, 1, 2, 0, '2016-02-27 09:11:56', '2016-02-27 09:11:57');
/*!40000 ALTER TABLE `rainlab_blog_categories` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_blog_posts
DROP TABLE IF EXISTS `rainlab_blog_posts`;
CREATE TABLE IF NOT EXISTS `rainlab_blog_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `content_html` text COLLATE utf8_unicode_ci,
  `published_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `rainlab_blog_posts_user_id_index` (`user_id`),
  KEY `rainlab_blog_posts_slug_index` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_blog_posts: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_blog_posts` DISABLE KEYS */;
INSERT INTO `rainlab_blog_posts` (`id`, `user_id`, `title`, `slug`, `excerpt`, `content`, `content_html`, `published_at`, `published`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'First blog post', 'first-blog-post', 'The first ever blog post is here. It might be a good idea to update this post with some more relevant content.', 'This is your first ever **blog post**! It might be a good idea to update this post with some more relevant content.\r\n\r\nYou can edit this content by selecting **Blog** from the administration back-end menu.\r\n\r\n*Enjoy the good times!*', '<p>This is your first ever <strong>blog post</strong>! It might be a good idea to update this post with some more relevant content.</p>\n<p>You can edit this content by selecting <strong>Blog</strong> from the administration back-end menu.</p>\n<p><em>Enjoy the good times!</em></p>', '2016-02-27 00:00:00', 1, '2016-02-27 09:11:56', '2016-03-09 06:10:15');
/*!40000 ALTER TABLE `rainlab_blog_posts` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_blog_posts_categories
DROP TABLE IF EXISTS `rainlab_blog_posts_categories`;
CREATE TABLE IF NOT EXISTS `rainlab_blog_posts_categories` (
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_blog_posts_categories: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_blog_posts_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_blog_posts_categories` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_forum_channels
DROP TABLE IF EXISTS `rainlab_forum_channels`;
CREATE TABLE IF NOT EXISTS `rainlab_forum_channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `count_topics` int(11) NOT NULL DEFAULT '0',
  `count_posts` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `is_moderated` tinyint(1) NOT NULL DEFAULT '0',
  `embed_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rainlab_forum_channels_slug_unique` (`slug`),
  KEY `rainlab_forum_channels_parent_id_index` (`parent_id`),
  KEY `rainlab_forum_channels_embed_code_index` (`embed_code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_forum_channels: ~9 rows (approximately)
/*!40000 ALTER TABLE `rainlab_forum_channels` DISABLE KEYS */;
INSERT INTO `rainlab_forum_channels` (`id`, `parent_id`, `title`, `slug`, `description`, `nest_left`, `nest_right`, `nest_depth`, `count_topics`, `count_posts`, `created_at`, `updated_at`, `is_hidden`, `is_moderated`, `embed_code`) VALUES
	(1, NULL, 'Channel Orange', 'channel-orange', 'A root level forum channel', 1, 12, 0, 0, 0, '2016-02-27 09:12:02', '2016-02-27 09:12:06', 0, 0, NULL),
	(2, 1, 'Autumn Leaves', 'autumn-leaves', 'Disccusion about the season of falling leaves.', 2, 9, 1, 0, 0, '2016-02-27 09:12:02', '2016-02-27 09:12:05', 0, 0, NULL),
	(3, 2, 'September', 'september', 'The start of the fall season.', 3, 4, 2, 0, 0, '2016-02-27 09:12:03', '2016-02-27 09:12:03', 0, 0, NULL),
	(4, 2, 'October', 'october', 'The middle of the fall season.', 5, 6, 2, 0, 0, '2016-02-27 09:12:04', '2016-02-27 09:12:05', 0, 0, NULL),
	(5, 2, 'November', 'november', 'The end of the fall season.', 7, 8, 2, 0, 0, '2016-02-27 09:12:05', '2016-02-27 09:12:06', 0, 0, NULL),
	(6, 1, 'Summer Breeze', 'summer-breeze', 'Disccusion about the wind at the ocean.', 10, 11, 1, 0, 0, '2016-02-27 09:12:06', '2016-02-27 09:12:06', 0, 0, NULL),
	(7, NULL, 'Channel Green', 'channel-green', 'A root level forum channel', 13, 18, 0, 0, 0, '2016-02-27 09:12:06', '2016-02-27 09:12:07', 0, 0, NULL),
	(8, 7, 'Winter Snow', 'winter-snow', 'Disccusion about the frosty snow flakes.', 14, 15, 1, 0, 0, '2016-02-27 09:12:07', '2016-02-27 09:12:07', 0, 0, NULL),
	(9, 7, 'Spring Trees', 'spring-trees', 'Disccusion about the blooming gardens.', 16, 17, 1, 0, 0, '2016-02-27 09:12:07', '2016-02-27 09:12:07', 0, 0, NULL);
/*!40000 ALTER TABLE `rainlab_forum_channels` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_forum_channel_watches
DROP TABLE IF EXISTS `rainlab_forum_channel_watches`;
CREATE TABLE IF NOT EXISTS `rainlab_forum_channel_watches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) unsigned DEFAULT NULL,
  `count_topics` int(11) NOT NULL DEFAULT '0',
  `watched_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_forum_channel_watches_channel_id_index` (`channel_id`),
  KEY `rainlab_forum_channel_watches_member_id_index` (`member_id`),
  KEY `rainlab_forum_channel_watches_count_topics_index` (`count_topics`),
  KEY `rainlab_forum_channel_watches_watched_at_index` (`watched_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_forum_channel_watches: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_forum_channel_watches` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_forum_channel_watches` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_forum_members
DROP TABLE IF EXISTS `rainlab_forum_members`;
CREATE TABLE IF NOT EXISTS `rainlab_forum_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count_posts` int(11) NOT NULL DEFAULT '0',
  `count_topics` int(11) NOT NULL DEFAULT '0',
  `last_active_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_moderator` tinyint(1) NOT NULL DEFAULT '0',
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rainlab_forum_members_user_id_index` (`user_id`),
  KEY `rainlab_forum_members_count_posts_index` (`count_posts`),
  KEY `rainlab_forum_members_count_topics_index` (`count_topics`),
  KEY `rainlab_forum_members_last_active_at_index` (`last_active_at`),
  KEY `rainlab_forum_members_is_moderator_index` (`is_moderator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_forum_members: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_forum_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_forum_members` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_forum_posts
DROP TABLE IF EXISTS `rainlab_forum_posts`;
CREATE TABLE IF NOT EXISTS `rainlab_forum_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `content_html` text COLLATE utf8_unicode_ci,
  `topic_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) unsigned DEFAULT NULL,
  `edit_user_id` int(11) DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `rainlab_forum_posts_topic_id_index` (`topic_id`),
  KEY `rainlab_forum_posts_member_id_index` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_forum_posts: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_forum_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_forum_posts` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_forum_topics
DROP TABLE IF EXISTS `rainlab_forum_topics`;
CREATE TABLE IF NOT EXISTS `rainlab_forum_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `channel_id` int(10) unsigned NOT NULL,
  `start_member_id` int(11) DEFAULT NULL,
  `last_post_id` int(11) DEFAULT NULL,
  `last_post_member_id` int(11) DEFAULT NULL,
  `last_post_at` datetime DEFAULT NULL,
  `is_private` tinyint(1) NOT NULL DEFAULT '0',
  `is_sticky` tinyint(1) NOT NULL DEFAULT '0',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `count_posts` int(11) NOT NULL DEFAULT '0',
  `count_views` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `embed_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rainlab_forum_topics_slug_unique` (`slug`),
  KEY `sticky_post_time` (`is_sticky`,`last_post_at`),
  KEY `rainlab_forum_topics_channel_id_index` (`channel_id`),
  KEY `rainlab_forum_topics_start_member_id_index` (`start_member_id`),
  KEY `rainlab_forum_topics_last_post_at_index` (`last_post_at`),
  KEY `rainlab_forum_topics_is_private_index` (`is_private`),
  KEY `rainlab_forum_topics_is_locked_index` (`is_locked`),
  KEY `rainlab_forum_topics_count_posts_index` (`count_posts`),
  KEY `rainlab_forum_topics_count_views_index` (`count_views`),
  KEY `rainlab_forum_topics_embed_code_index` (`embed_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_forum_topics: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_forum_topics` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_forum_topics` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_forum_topic_followers
DROP TABLE IF EXISTS `rainlab_forum_topic_followers`;
CREATE TABLE IF NOT EXISTS `rainlab_forum_topic_followers` (
  `topic_id` int(10) unsigned NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`topic_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_forum_topic_followers: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_forum_topic_followers` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_forum_topic_followers` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_forum_topic_watches
DROP TABLE IF EXISTS `rainlab_forum_topic_watches`;
CREATE TABLE IF NOT EXISTS `rainlab_forum_topic_watches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) unsigned DEFAULT NULL,
  `count_posts` int(11) NOT NULL DEFAULT '0',
  `watched_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rainlab_forum_topic_watches_topic_id_index` (`topic_id`),
  KEY `rainlab_forum_topic_watches_member_id_index` (`member_id`),
  KEY `rainlab_forum_topic_watches_count_posts_index` (`count_posts`),
  KEY `rainlab_forum_topic_watches_watched_at_index` (`watched_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_forum_topic_watches: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_forum_topic_watches` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_forum_topic_watches` ENABLE KEYS */;


-- Dumping structure for table octobercms.rainlab_user_mail_blockers
DROP TABLE IF EXISTS `rainlab_user_mail_blockers`;
CREATE TABLE IF NOT EXISTS `rainlab_user_mail_blockers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `rainlab_user_mail_blockers_email_index` (`email`),
  KEY `rainlab_user_mail_blockers_template_index` (`template`),
  KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.rainlab_user_mail_blockers: ~0 rows (approximately)
/*!40000 ALTER TABLE `rainlab_user_mail_blockers` DISABLE KEYS */;
/*!40000 ALTER TABLE `rainlab_user_mail_blockers` ENABLE KEYS */;


-- Dumping structure for table octobercms.sessions
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_event_logs
DROP TABLE IF EXISTS `system_event_logs`;
CREATE TABLE IF NOT EXISTS `system_event_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `details` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `system_event_logs_level_index` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_event_logs: ~44 rows (approximately)
/*!40000 ALTER TABLE `system_event_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_event_logs` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_files
DROP TABLE IF EXISTS `system_files`;
CREATE TABLE IF NOT EXISTS `system_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disk_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `system_files_field_index` (`field`),
  KEY `system_files_attachment_id_index` (`attachment_id`),
  KEY `system_files_attachment_type_index` (`attachment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_files: ~1 rows (approximately)
/*!40000 ALTER TABLE `system_files` DISABLE KEYS */;
INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
	(1, '56e419fb2d222877825652.png', 'Drawing (1).png', 6153, 'image/png', NULL, NULL, 'logo', '2', 'Backend\\Models\\BrandSettings', 1, 1, '2016-03-12 13:30:35', '2016-03-12 13:30:54');
/*!40000 ALTER TABLE `system_files` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_mail_layouts
DROP TABLE IF EXISTS `system_mail_layouts`;
CREATE TABLE IF NOT EXISTS `system_mail_layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8_unicode_ci,
  `content_text` text COLLATE utf8_unicode_ci,
  `content_css` text COLLATE utf8_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_mail_layouts: ~2 rows (approximately)
/*!40000 ALTER TABLE `system_mail_layouts` DISABLE KEYS */;
INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `created_at`, `updated_at`) VALUES
	(1, 'Default', 'default', '<html>\n    <head>\n        <style type="text/css" media="screen">\n            {{ css|raw }}\n        </style>\n    </head>\n    <body>\n        {{ content|raw }}\n    </body>\n</html>', '{{ content|raw }}', 'a, a:hover {\n    text-decoration: none;\n    color: #0862A2;\n    font-weight: bold;\n}\n\ntd, tr, th, table {\n    padding: 0px;\n    margin: 0px;\n}\n\np {\n    margin: 10px 0;\n}', 1, '2016-02-18 09:23:41', '2016-02-18 09:23:41'),
	(2, 'System', 'system', '<html>\n    <head>\n        <style type="text/css" media="screen">\n            {{ css|raw }}\n        </style>\n    </head>\n    <body>\n        {{ content|raw }}\n        <hr />\n        <p>This is an automatic message. Please do not reply to it.</p>\n    </body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', 'a, a:hover {\n    text-decoration: none;\n    color: #0862A2;\n    font-weight: bold;\n}\n\ntd, tr, th, table {\n    padding: 0px;\n    margin: 0px;\n}\n\np {\n    margin: 10px 0;\n}', 1, '2016-02-18 09:23:42', '2016-02-18 09:23:42');
/*!40000 ALTER TABLE `system_mail_layouts` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_mail_templates
DROP TABLE IF EXISTS `system_mail_templates`;
CREATE TABLE IF NOT EXISTS `system_mail_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content_html` text COLLATE utf8_unicode_ci,
  `content_text` text COLLATE utf8_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `system_mail_templates_layout_id_index` (`layout_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_mail_templates: ~7 rows (approximately)
/*!40000 ALTER TABLE `system_mail_templates` DISABLE KEYS */;
INSERT INTO `system_mail_templates` (`id`, `code`, `subject`, `description`, `content_html`, `content_text`, `layout_id`, `is_custom`, `created_at`, `updated_at`) VALUES
	(1, 'backend::mail.invite', NULL, 'Invitation for newly created administrators.', NULL, NULL, 2, 0, '2016-02-19 09:39:07', '2016-02-19 09:39:07'),
	(2, 'backend::mail.restore', NULL, 'Password reset instructions for backend-end administrators.', NULL, NULL, 2, 0, '2016-02-19 09:39:07', '2016-02-19 09:39:07'),
	(3, 'rainlab.user::mail.activate', NULL, 'Activation email sent to new users.', NULL, NULL, 1, 0, '2016-02-19 09:39:07', '2016-02-19 09:39:07'),
	(4, 'rainlab.user::mail.welcome', NULL, 'Welcome email sent when a user is activated.', NULL, NULL, 1, 0, '2016-02-19 09:39:07', '2016-02-19 09:39:07'),
	(5, 'rainlab.user::mail.restore', NULL, 'Password reset instructions for front-end users.', NULL, NULL, 1, 0, '2016-02-19 09:39:07', '2016-02-19 09:39:07'),
	(6, 'rainlab.user::mail.new_user', NULL, 'Sent to administrators when a new user joins.', NULL, NULL, 2, 0, '2016-02-19 09:39:07', '2016-02-19 09:39:07'),
	(7, 'rainlab.user::mail.reactivate', NULL, 'Notification for users who reactivate their account.', NULL, NULL, 1, 0, '2016-02-19 09:39:07', '2016-02-19 09:39:07');
/*!40000 ALTER TABLE `system_mail_templates` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_parameters
DROP TABLE IF EXISTS `system_parameters`;
CREATE TABLE IF NOT EXISTS `system_parameters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `item_index` (`namespace`,`group`,`item`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_parameters: ~3 rows (approximately)
/*!40000 ALTER TABLE `system_parameters` DISABLE KEYS */;
INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
	(1, 'system', 'update', 'count', '0'),
	(2, 'system', 'core', 'hash', '"601f612b7884ae69f22a681fcf5070f7"'),
	(3, 'system', 'core', 'build', '"316"'),
	(4, 'system', 'update', 'retry', '1457875786'),
	(5, 'cms', 'theme', 'active', '"responsiv-flat"'),
	(6, 'system', 'theme', 'history', '{"RainLab.Vanilla":"rainlab-vanilla"}');
/*!40000 ALTER TABLE `system_parameters` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_plugin_history
DROP TABLE IF EXISTS `system_plugin_history`;
CREATE TABLE IF NOT EXISTS `system_plugin_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `system_plugin_history_code_index` (`code`),
  KEY `system_plugin_history_type_index` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_plugin_history: ~111 rows (approximately)
/*!40000 ALTER TABLE `system_plugin_history` DISABLE KEYS */;
INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
	(1, 'RainLab.User', 'script', '1.0.1', 'create_users_table.php', '2016-02-19 09:07:05'),
	(2, 'RainLab.User', 'script', '1.0.1', 'create_throttle_table.php', '2016-02-19 09:07:06'),
	(3, 'RainLab.User', 'comment', '1.0.1', 'Initialize plugin.', '2016-02-19 09:07:06'),
	(4, 'RainLab.User', 'comment', '1.0.2', 'Seed tables.', '2016-02-19 09:07:06'),
	(5, 'RainLab.User', 'comment', '1.0.3', 'Translated hard-coded text to language strings.', '2016-02-19 09:07:06'),
	(6, 'RainLab.User', 'comment', '1.0.4', 'Improvements to user-interface for Location manager.', '2016-02-19 09:07:06'),
	(7, 'RainLab.User', 'comment', '1.0.5', 'Added contact details for users.', '2016-02-19 09:07:06'),
	(8, 'RainLab.User', 'script', '1.0.6', 'create_mail_blockers_table.php', '2016-02-19 09:07:06'),
	(9, 'RainLab.User', 'comment', '1.0.6', 'Added Mail Blocker utility so users can block specific mail templates.', '2016-02-19 09:07:06'),
	(10, 'RainLab.User', 'comment', '1.0.7', 'Add back-end Settings page.', '2016-02-19 09:07:06'),
	(11, 'RainLab.User', 'comment', '1.0.8', 'Updated the Settings page.', '2016-02-19 09:07:06'),
	(12, 'RainLab.User', 'comment', '1.0.9', 'Adds new welcome mail message for users and administrators.', '2016-02-19 09:07:06'),
	(13, 'RainLab.User', 'comment', '1.0.10', 'Adds administrator-only activation mode.', '2016-02-19 09:07:06'),
	(14, 'RainLab.User', 'script', '1.0.11', 'users_add_login_column.php', '2016-02-19 09:07:07'),
	(15, 'RainLab.User', 'comment', '1.0.11', 'Users now have an optional login field that defaults to the email field.', '2016-02-19 09:07:07'),
	(16, 'RainLab.User', 'script', '1.0.12', 'users_rename_login_to_username.php', '2016-02-19 09:07:08'),
	(17, 'RainLab.User', 'comment', '1.0.12', 'Create a dedicated setting for choosing the login mode.', '2016-02-19 09:07:08'),
	(18, 'RainLab.User', 'comment', '1.0.13', 'Minor fix to the Account sign in logic.', '2016-02-19 09:07:08'),
	(19, 'RainLab.User', 'comment', '1.0.14', 'Minor improvements to the code.', '2016-02-19 09:07:08'),
	(20, 'RainLab.User', 'script', '1.0.15', 'users_add_surname.php', '2016-02-19 09:07:08'),
	(21, 'RainLab.User', 'comment', '1.0.15', 'Adds last name column to users table (surname).', '2016-02-19 09:07:08'),
	(22, 'RainLab.User', 'comment', '1.0.16', 'Require permissions for settings page too.', '2016-02-19 09:07:08'),
	(23, 'RainLab.User', 'comment', '1.1.0', '!!! Profile fields and Locations have been removed.', '2016-02-19 09:07:08'),
	(24, 'RainLab.User', 'script', '1.1.1', 'create_user_groups_table.php', '2016-02-19 09:07:08'),
	(25, 'RainLab.User', 'script', '1.1.1', 'seed_user_groups_table.php', '2016-02-19 09:07:08'),
	(26, 'RainLab.User', 'comment', '1.1.1', 'Users can now be added to groups.', '2016-02-19 09:07:08'),
	(27, 'RainLab.User', 'comment', '1.1.2', 'A raw URL can now be passed as the redirect property in the Account component.', '2016-02-19 09:07:08'),
	(28, 'RainLab.User', 'comment', '1.1.3', 'Adds a super user flag to the users table, reserved for future use.', '2016-02-19 09:07:09'),
	(29, 'RainLab.User', 'comment', '1.1.4', 'User list can be filtered by the group they belong to.', '2016-02-19 09:07:09'),
	(30, 'RainLab.User', 'comment', '1.1.5', 'Adds a new permission to hide the User settings menu item.', '2016-02-19 09:07:09'),
	(31, 'RainLab.User', 'script', '1.2.0', 'users_add_deleted_at.php', '2016-02-19 09:07:09'),
	(32, 'RainLab.User', 'comment', '1.2.0', 'Users can now deactivate their own accounts.', '2016-02-19 09:07:09'),
	(33, 'RainLab.User', 'comment', '1.2.1', 'New feature for checking if a user is recently active/online.', '2016-02-19 09:07:09'),
	(34, 'RainLab.Pages', 'comment', '1.0.1', 'Implemented the static pages management and the Static Page component.', '2016-02-19 09:07:57'),
	(35, 'RainLab.Pages', 'comment', '1.0.2', 'Fixed the page preview URL.', '2016-02-19 09:07:57'),
	(36, 'RainLab.Pages', 'comment', '1.0.3', 'Implemented menus.', '2016-02-19 09:07:57'),
	(37, 'RainLab.Pages', 'comment', '1.0.4', 'Implemented the content block management and placeholder support.', '2016-02-19 09:07:57'),
	(38, 'RainLab.Pages', 'comment', '1.0.5', 'Added support for the Sitemap plugin.', '2016-02-19 09:07:57'),
	(39, 'RainLab.Pages', 'comment', '1.0.6', 'Minor updates to the internal API.', '2016-02-19 09:07:57'),
	(40, 'RainLab.Pages', 'comment', '1.0.7', 'Added the Snippets feature.', '2016-02-19 09:07:57'),
	(41, 'RainLab.Pages', 'comment', '1.0.8', 'Minor improvements to the code.', '2016-02-19 09:07:58'),
	(42, 'RainLab.Pages', 'comment', '1.0.9', 'Fixes issue where Snippet tab is missing from the Partials form.', '2016-02-19 09:07:58'),
	(43, 'RainLab.Pages', 'comment', '1.0.10', 'Add translations for various locales.', '2016-02-19 09:07:58'),
	(44, 'RainLab.Pages', 'comment', '1.0.11', 'Fixes issue where placeholders tabs were missing from Page form.', '2016-02-19 09:07:58'),
	(45, 'RainLab.Pages', 'comment', '1.0.12', 'Implement Media Manager support.', '2016-02-19 09:07:58'),
	(46, 'RainLab.Pages', 'script', '1.1.0', 'snippets_rename_viewbag_properties.php', '2016-02-19 09:07:58'),
	(47, 'RainLab.Pages', 'comment', '1.1.0', 'Adds meta title and description to pages. Adds |staticPage filter.', '2016-02-19 09:07:58'),
	(48, 'RainLab.Pages', 'comment', '1.1.1', 'Add support for Syntax Fields.', '2016-02-19 09:07:58'),
	(49, 'RainLab.Pages', 'comment', '1.1.2', 'Static Breadcrumbs component now respects the hide from navigation setting.', '2016-02-19 09:07:58'),
	(50, 'RainLab.Pages', 'comment', '1.1.3', 'Minor back-end styling fix.', '2016-02-19 09:07:58'),
	(51, 'RainLab.Pages', 'comment', '1.1.4', 'Minor fix to the StaticPage component API.', '2016-02-19 09:07:58'),
	(52, 'RainLab.Pages', 'comment', '1.1.5', 'Fixes bug when using syntax fields.', '2016-02-19 09:07:58'),
	(53, 'RainLab.Pages', 'comment', '1.1.6', 'Minor styling fix to the back-end UI.', '2016-02-19 09:07:58'),
	(54, 'RainLab.Pages', 'comment', '1.1.7', 'Improved menu item form to include css class, open in a new window and hidden flag.', '2016-02-19 09:07:59'),
	(55, 'RainLab.Pages', 'comment', '1.1.8', 'Improved the output of snippet partials when saved.', '2016-02-19 09:07:59'),
	(56, 'RainLab.Pages', 'comment', '1.1.9', 'Minor update to snippet inspector internal API.', '2016-02-19 09:07:59'),
	(57, 'RainLab.Pages', 'comment', '1.1.10', 'Fixes a bug where selecting a layout causes permanent unsaved changes.', '2016-02-19 09:07:59'),
	(58, 'RainLab.Pages', 'comment', '1.1.11', 'Add support for repeater syntax field.', '2016-02-19 09:07:59'),
	(59, 'RainLab.Blog', 'script', '1.0.1', 'create_posts_table.php', '2016-02-27 09:11:55'),
	(60, 'RainLab.Blog', 'script', '1.0.1', 'create_categories_table.php', '2016-02-27 09:11:56'),
	(61, 'RainLab.Blog', 'script', '1.0.1', 'seed_all_tables.php', '2016-02-27 09:11:57'),
	(62, 'RainLab.Blog', 'comment', '1.0.1', 'Initialize plugin.', '2016-02-27 09:11:57'),
	(63, 'RainLab.Blog', 'comment', '1.0.2', 'Added the processed HTML content column to the posts table.', '2016-02-27 09:11:57'),
	(64, 'RainLab.Blog', 'comment', '1.0.3', 'Category component has been merged with Posts component.', '2016-02-27 09:11:57'),
	(65, 'RainLab.Blog', 'comment', '1.0.4', 'Improvements to the Posts list management UI.', '2016-02-27 09:11:57'),
	(66, 'RainLab.Blog', 'comment', '1.0.5', 'Removes the Author column from blog post list.', '2016-02-27 09:11:57'),
	(67, 'RainLab.Blog', 'comment', '1.0.6', 'Featured images now appear in the Post component.', '2016-02-27 09:11:57'),
	(68, 'RainLab.Blog', 'comment', '1.0.7', 'Added support for the Static Pages menus.', '2016-02-27 09:11:57'),
	(69, 'RainLab.Blog', 'comment', '1.0.8', 'Added total posts to category list.', '2016-02-27 09:11:57'),
	(70, 'RainLab.Blog', 'comment', '1.0.9', 'Added support for the Sitemap plugin.', '2016-02-27 09:11:57'),
	(71, 'RainLab.Blog', 'comment', '1.0.10', 'Added permission to prevent users from seeing posts they did not create.', '2016-02-27 09:11:57'),
	(72, 'RainLab.Blog', 'comment', '1.0.11', 'Deprecate "idParam" component property in favour of "slug" property.', '2016-02-27 09:11:57'),
	(73, 'RainLab.Blog', 'comment', '1.0.12', 'Fixes issue where images cannot be uploaded caused by latest Markdown library.', '2016-02-27 09:11:57'),
	(74, 'RainLab.Blog', 'comment', '1.0.13', 'Fixes problem with providing pages to Sitemap and Pages plugins.', '2016-02-27 09:11:57'),
	(75, 'RainLab.Blog', 'comment', '1.0.14', 'Add support for CSRF protection feature added to core.', '2016-02-27 09:11:57'),
	(76, 'RainLab.Blog', 'comment', '1.1.0', 'Replaced the Post editor with the new core Markdown editor.', '2016-02-27 09:11:57'),
	(77, 'RainLab.Blog', 'comment', '1.1.1', 'Posts can now be imported and exported.', '2016-02-27 09:11:57'),
	(78, 'RainLab.Blog', 'comment', '1.1.2', 'Posts are no longer visible if the published date has not passed.', '2016-02-27 09:11:57'),
	(79, 'RainLab.Blog', 'comment', '1.1.3', 'Added a New Post shortcut button to the blog menu.', '2016-02-27 09:11:57'),
	(80, 'RainLab.Blog', 'script', '1.2.0', 'categories_add_nested_fields.php', '2016-02-27 09:11:58'),
	(81, 'RainLab.Blog', 'comment', '1.2.0', 'Categories now support nesting.', '2016-02-27 09:11:58'),
	(82, 'RainLab.Blog', 'comment', '1.2.1', 'Post slugs now must be unique.', '2016-02-27 09:11:58'),
	(83, 'RainLab.Blog', 'comment', '1.2.2', 'Fixes issue on new installs.', '2016-02-27 09:11:58'),
	(84, 'RainLab.Forum', 'script', '1.0.1', 'create_channels_table.php', '2016-02-27 09:11:59'),
	(85, 'RainLab.Forum', 'script', '1.0.1', 'create_posts_table.php', '2016-02-27 09:12:00'),
	(86, 'RainLab.Forum', 'script', '1.0.1', 'create_topics_table.php', '2016-02-27 09:12:01'),
	(87, 'RainLab.Forum', 'script', '1.0.1', 'create_members_table.php', '2016-02-27 09:12:01'),
	(88, 'RainLab.Forum', 'script', '1.0.1', 'seed_all_tables.php', '2016-02-27 09:12:07'),
	(89, 'RainLab.Forum', 'comment', '1.0.1', 'First version of Forum', '2016-02-27 09:12:07'),
	(90, 'RainLab.Forum', 'script', '1.0.2', 'create_topic_watches_table.php', '2016-02-27 09:12:08'),
	(91, 'RainLab.Forum', 'comment', '1.0.2', 'Add unread flags to topics', '2016-02-27 09:12:08'),
	(92, 'RainLab.Forum', 'script', '1.0.3', 'members_add_mod_and_ban.php', '2016-02-27 09:12:08'),
	(93, 'RainLab.Forum', 'comment', '1.0.3', 'Users can now be made moderators or be banned', '2016-02-27 09:12:08'),
	(94, 'RainLab.Forum', 'script', '1.0.4', 'channels_add_hidden_and_moderated.php', '2016-02-27 09:12:08'),
	(95, 'RainLab.Forum', 'comment', '1.0.4', 'Channels can now be hidden or moderated', '2016-02-27 09:12:08'),
	(96, 'RainLab.Forum', 'script', '1.0.5', 'add_embed_code.php', '2016-02-27 09:12:09'),
	(97, 'RainLab.Forum', 'comment', '1.0.5', 'Introduced topic and channel embedding', '2016-02-27 09:12:09'),
	(98, 'RainLab.Forum', 'script', '1.0.6', 'create_channel_watches_table.php', '2016-02-27 09:12:10'),
	(99, 'RainLab.Forum', 'comment', '1.0.6', 'Add unread flags to channels', '2016-02-27 09:12:10'),
	(100, 'RainLab.Forum', 'script', '1.0.7', 'create_topic_followers_table.php', '2016-02-27 09:12:10'),
	(101, 'RainLab.Forum', 'comment', '1.0.7', 'Forum members can now follow topics', '2016-02-27 09:12:10'),
	(102, 'RainLab.Forum', 'comment', '1.0.8', 'Added Channel name to the Topics component view', '2016-02-27 09:12:10'),
	(103, 'RainLab.Forum', 'comment', '1.0.9', 'Updated the Settings page', '2016-02-27 09:12:10'),
	(104, 'RainLab.Forum', 'comment', '1.0.10', 'Users can now report spammers who can be banned by moderators.', '2016-02-27 09:12:10'),
	(105, 'RainLab.Forum', 'comment', '1.0.11', 'Users can now quote other posts.', '2016-02-27 09:12:10'),
	(106, 'RainLab.Forum', 'comment', '1.0.12', 'Improve support for CDN asset hosting.', '2016-02-27 09:12:10'),
	(107, 'RainLab.Forum', 'comment', '1.0.13', 'Fixes a bug where channels cannot be selected in the Embed component inspector.', '2016-02-27 09:12:10'),
	(108, 'RainLab.Forum', 'comment', '1.0.14', 'Improve the pagination code used in the component default markup.', '2016-02-27 09:12:10'),
	(109, 'RainLab.Forum', 'comment', '1.0.15', 'When a User is deleted, their Member profile and posts is also deleted.', '2016-02-27 09:12:10'),
	(110, 'RainLab.Forum', 'comment', '1.0.16', 'Posting topics is now throttled allowing 3 new topics every 15 minutes.', '2016-02-27 09:12:10'),
	(111, 'RainLab.Forum', 'comment', '1.0.17', 'Update channel reorder page to new system reordering feature.', '2016-02-27 09:12:10'),
	(112, 'Tb.Events', 'script', '1.0.1', 'create_batch_table.php', '2016-03-08 11:43:52'),
	(113, 'Tb.Events', 'comment', '1.0.1', 'Initialize plugin.', '2016-03-08 11:43:52');
/*!40000 ALTER TABLE `system_plugin_history` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_plugin_versions
DROP TABLE IF EXISTS `system_plugin_versions`;
CREATE TABLE IF NOT EXISTS `system_plugin_versions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `system_plugin_versions_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_plugin_versions: ~4 rows (approximately)
/*!40000 ALTER TABLE `system_plugin_versions` DISABLE KEYS */;
INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
	(1, 'RainLab.User', '1.2.1', '2016-02-19 09:07:09', 0, 0),
	(2, 'RainLab.Pages', '1.1.11', '2016-02-19 09:07:59', 0, 0),
	(3, 'RainLab.Blog', '1.2.2', '2016-02-27 09:11:58', 0, 0),
	(4, 'RainLab.Forum', '1.0.17', '2016-02-27 09:12:10', 0, 0),
	(5, 'Tb.Events', '1.0.1', '2016-03-08 11:43:52', 0, 0);
/*!40000 ALTER TABLE `system_plugin_versions` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_request_logs
DROP TABLE IF EXISTS `system_request_logs`;
CREATE TABLE IF NOT EXISTS `system_request_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_request_logs: ~1 rows (approximately)
/*!40000 ALTER TABLE `system_request_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_request_logs` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_revisions
DROP TABLE IF EXISTS `system_revisions`;
CREATE TABLE IF NOT EXISTS `system_revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cast` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8_unicode_ci,
  `new_value` text COLLATE utf8_unicode_ci,
  `revisionable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  KEY `system_revisions_user_id_index` (`user_id`),
  KEY `system_revisions_field_index` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_revisions: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_revisions` ENABLE KEYS */;


-- Dumping structure for table octobercms.system_settings
DROP TABLE IF EXISTS `system_settings`;
CREATE TABLE IF NOT EXISTS `system_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `system_settings_item_index` (`item`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.system_settings: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_settings` DISABLE KEYS */;
INSERT INTO `system_settings` (`id`, `item`, `value`) VALUES
	(1, 'user_settings', '{"require_activation":"1","activate_mode":"user","use_throttle":"1","allow_registration":"1","welcome_template":"rainlab.user::mail.welcome","login_attribute":"email"}'),
	(2, 'backend_brand_settings', '{"app_name":"Student Management","app_tagline":"Login","primary_color_light":"#e74c3c","primary_color_dark":"#c0392b","secondary_color_light":"#e74c3c","secondary_color_dark":"#c0392b","custom_css":""}');
/*!40000 ALTER TABLE `system_settings` ENABLE KEYS */;


-- Dumping structure for table octobercms.tb_event_batches
DROP TABLE IF EXISTS `tb_event_batches`;
CREATE TABLE IF NOT EXISTS `tb_event_batches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tb_event_batches_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.tb_event_batches: ~3 rows (approximately)
/*!40000 ALTER TABLE `tb_event_batches` DISABLE KEYS */;
INSERT INTO `tb_event_batches` (`id`, `name`, `description`, `address`, `city`, `state`, `zipcode`, `country`, `lat`, `long`, `start_date`, `end_date`, `user_id`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Dance', 'Dance floor', 'Prahlad nagar', 'Ahmedabad', 'Gujarat', '387890', 'India', NULL, NULL, '2016-03-12 19:01:00', '2016-03-14 19:01:00', 1, 1, '2016-03-08 19:38:10', '2016-03-09 08:33:24', NULL),
	(2, 'Test1', 'TEST BATCH1', 'SHAH PUR', 'Ahmedabad', 'gujarat', '387890', 'IN', NULL, NULL, '2016-03-08 19:01:48', '2016-03-08 19:01:50', 1, 0, '2016-03-08 19:38:12', '2016-03-08 19:38:08', NULL),
	(4, 'Dance & Drama', 'test', 'B-25, Nilkanth Society, B/H. Bajarang Ashram, Hirawadi Road, Thakkarbapa nagar', 'Ahmedabad', 'Gujarat', '382350', 'India', NULL, NULL, '2016-03-11 00:00:00', '2016-03-12 01:05:00', 1, 1, '2016-03-09 08:52:23', '2016-03-12 06:18:04', NULL);
/*!40000 ALTER TABLE `tb_event_batches` ENABLE KEYS */;


-- Dumping structure for table octobercms.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_login_unique` (`username`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`),
  KEY `users_login_index` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `activated_at`, `last_login`, `created_at`, `updated_at`, `username`, `surname`, `deleted_at`) VALUES
	(1, 'BU', 'business@owner.com', '$2y$10$KoXWtThdiOPGXrrspS3O8eXjfIxfUf67/2/3KGC7AC8LrkKlDHUvm', NULL, NULL, NULL, NULL, 0, NULL, NULL, '2016-02-27 08:58:14', '2016-02-27 08:58:14', 'business@owner.com', 'owner', NULL),
	(2, 'tris', 'tris@test.com', '$2y$10$iVEai/TRxWvbBOo6RPzp5eFTx7pt1mEQDBrjf.xFnUMS8pX7zTWpm', 'kO0wNAAgkLKQVQJGBbWNG13m4Am0ea546rpVF3bXcn', NULL, NULL, NULL, 0, NULL, NULL, '2016-02-27 09:15:00', '2016-02-27 09:15:00', 'tris@test.com', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table octobercms.users_groups
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `user_group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.users_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`user_id`, `user_group_id`) VALUES
	(1, 3);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;


-- Dumping structure for table octobercms.user_groups
DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_groups_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.user_groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin', 'Administrator', '2016-02-19 09:07:08', '2016-02-25 06:10:04'),
	(2, 'Student', 'student', 'Student', '2016-02-19 09:09:42', '2016-02-25 06:09:03'),
	(3, 'Business Owner', 'owner', 'Business Owner', '2016-02-25 06:14:38', '2016-02-27 08:12:26');
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;


-- Dumping structure for table octobercms.user_throttle
DROP TABLE IF EXISTS `user_throttle`;
CREATE TABLE IF NOT EXISTS `user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_throttle_user_id_index` (`user_id`),
  KEY `user_throttle_ip_address_index` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table octobercms.user_throttle: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_throttle` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
