<?php

namespace Tb\BackendCustom;
use App;
use Event;
use Backend;
use System\Classes\PluginBase;

class BackendCustom extends \System\Classes\PluginBase {
	public function pluginDetails() {
		return [ 
				'name' => 'Backend Customization Plugin',
				'description' => 'Backend Customization',
				'author' => 'Bhaumik',
		];
	}
	
	public function boot() {
		
	}
}