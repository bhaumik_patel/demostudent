<?php
return [ 
		'plugin' => [ 
				'name' => 'Events',
				'description' => 'Front-end event management.',
				'tab' => 'Events',
				'access_batches' => 'Manage Classes' 
		],
		'batches' => [ 
				'menu_label' => 'Classes',
				'id' => 'Id',
				'name' => 'Name',
				'address' => 'Address',
				'city' => 'City',
				'state' => 'State',
				'zipcode' => 'Zipcode',
				'country' => 'Country',
				'lat' => 'Lat',
				'description' => 'Description',
				'start_date' => 'Start Date',
				'end_date' => 'End Date',
				'created_at' => 'Created At', 
				'updated_at' => 'Updated At',
				'is_active'  => 'Is Active',
				'user_id' => 'User Id',
				'list_title' => 'Manage Classes',
				'new_batch'  => 'New Class',
				'all_batches' => 'Classes',
				'return_to_list' => 'Retrurn to Classes',
				'label' => 'Classes',
				'create_title' => 'New Class',
				'update_title' => 'Update Class',
                                'delete_confirm' => 'Confirm to Delete',
		]
];