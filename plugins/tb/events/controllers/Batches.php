<?php

namespace Tb\Events\Controllers;

use Flash;
use Redirect;
use BackendMenu;
use Backend\Classes\Controller;
use ApplicationException;
use Tb\Events\Models\Batch;

class Batches extends Controller {

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];
    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $requiredPermissions = ['tb.events.access_batches', 'tb.events.access_batch_create', 'tb.events.access_batch_update'];

    public function __construct() {
        parent::__construct();

        BackendMenu::setContext('Tb.Events', 'events', 'events');
    }

    public function index() {
        $this->asExtension('ListController')->index();
    }
    
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $id) {
                if ((!$batch = Batch::find($id))) // || !$batch->canEdit($this->user))
                    continue;

                $batch->delete();
            }

            Flash::success('Successfully deleted those Classes.');
        }

        return $this->listRefresh();
    }

}
