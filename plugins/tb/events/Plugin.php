<?php

namespace Tb\Events;

use App;
use Event;
use Backend;
use System\Classes\PluginBase;

class Plugin extends \System\Classes\PluginBase {

    public function pluginDetails() {
        return [
            'name' => 'Events Plugin',
            'description' => 'Child event management features.',
            'author' => 'Bhaumik',
            'icon' => 'icon-leaf'
        ];
    }

    public function registerComponents() {
        return [
            'Tb\Events\Components\Batch' => 'Batch',
            'Tb\Events\Components\Countries' => 'Countries',
        ];
    }

    public function registerPermissions() {
        return [
            'tb.events.access_batches' => ['tab' => 'tb.events::lang.plugin.tab', 'label' => 'tb.events::lang.plugin.access_batches'],
        ];
    }

    public function registerNavigation() {
        return [
            'events' => [
                'label' => 'tb.events::lang.batches.menu_label',
                'url' => Backend::url('tb/events/batches'),
                'icon' => 'icon-user',
                'permissions' => ['tb.events.*'],
                'order' => 600,
            ]
        ];
    }

    public function boot() {
        
    }

}
