<?php 
namespace Tb\Events\Components;

use Cms\Classes\ComponentBase;

class Batch extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Tb.Events::lang.batches.name',
            'description' => 'Tb.Events::lang.batches.description'
        ];
    }
}