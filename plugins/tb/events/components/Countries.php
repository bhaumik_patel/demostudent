<?php

namespace Tb\Events\Components;

use Cms\Classes\ComponentBase;

class Countries extends ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Tb.Events::lang.countries.name',
            'description' => 'Tb.Events::lang.countries.description'
        ];
    }

    public function defineProperties() {
        return [
            'country' => [
                'title' => 'Country',
                'type' => 'dropdown',
                'default' => '',
                'placeholder' => 'Select a Country'
            ],
            'state' => [
                'title' => 'State',
                'type' => 'dropdown',
                'depends' => ['country'],
                'placeholder' => 'Select a state'
            ]
        ];
    }

    public static function getCountryOptions() {
        return [
            'in' => 'India',
            'us' => 'United States',
        ];
    }

    public static function getAllStates() {
        return [
            'us' => [
                'al' => 'Alabama',
                'ak' => 'Alaska',
                'ca' => 'California',
                'tx' => 'Texas',
                'ny' => 'New York',
            ],
            'in' => [
                'ap' => 'Andhra Pradesh',
                'gj' => 'Gujarat',
                'go' => 'Goa',
                'mp' => 'Madhya Pradesh',
                'up' => 'Uttar Pradesh',
            ],
        ];
    }

    public static function getStateOptions($countryCode = 'us') {
        $states = self::getAllStates();
        return (isset($states[$countryCode])) ? $states[$countryCode] : [];
    }

}
