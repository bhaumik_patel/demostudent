<?php

namespace Tb\Events\Models;

use App;
use Str;
use Html;
use Lang;
use Model;
use ValidationException;
use Backend\Models\User;
use DB;
use BackendAuth;
use Illuminate\Support\Facades\Request;
use Tb\Events\Components\Countries;

class Batches extends Model {

    use \October\Rain\Database\Traits\SoftDeleting;

    use \October\Rain\Database\Traits\Validation;

    /**
     *
     * @var string The database table used by the model.
     */
    protected $table = 'tb_event_batches';

    /**
     * Validation rules
     */
    public $rules = [
        'name' => 'required|between:6,255|unique:tb_event_batches',
        'description' => 'required|between:2,255',
        'address' => 'required|between:2,255',
        'city' => 'required|between:2,255',
        'state' => 'required|between:2,255',
        'country' => 'required|between:2,255',
        'zipcode' => 'required|between:2,10',
        'start_date' => 'required|date',
        'end_date' => 'required|date|after:start_date',
    ];

    /**
     *
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'desc',
        'start_date',
        'end_date',
        'city',
        'country',
        'lat',
        'long'
    ];

    
    /**
     * Before validation event
     * 
     * @return void
     */
    public function beforeValidate() {
        
    }

    /**
     * Before Save Batch 
     */
    public function beforeSave() {
        $user = BackendAuth::getUser();
        $this->user_id = $user->id;
    }

    public function listCountries($keyValue = null, $fieldName = null) {
        return \Tb\Events\Components\Countries::getCountryOptions();
    }

    public function getStateOptions() {
        return \Tb\Events\Components\Countries::getStateOptions();
    }

    public function listStates() {
        if($this->country!='') {
            $countryCode = $this->country;
        }else {
            $countryCode = Request::input('Batches.country'); // Load the country property value from POST
        }
        return Countries::getStateOptions($countryCode);
    }

    /**
     * After delete event
     * 
     * @return void
     */
    public function afterDelete() {
        if ($this->isSoftDelete()) {
            // Event::fire('tb.event.batches.deactivate', [$this]);
            return;
        }
        parent::afterDelete();
    }

    public function scopeIsActive($query) {
        return $query->where('is_active', 1);
    }

}
