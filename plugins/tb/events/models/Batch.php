<?php

namespace Tb\Events\Models;

use App;
use Str;
use Html;
use Lang;
use Model;
use ValidationException;
use Backend\Models\User;

use DB;

class Batch extends Model
{
    public $table = 'tb_event_batches';
}